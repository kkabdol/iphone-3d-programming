//
//  GLView.h
//  HelloArrow
//
//  Created by Seonghyeon Choe on 11/2/14.
//  Copyright (c) 2014 shaun. All rights reserved.
//

#import "IRenderingEngine.hpp"
#import <UIKit/UIKit.h>
#import <OpenGLES/EAGL.h>
#import <QuartzCore/QuartzCore.h>

@interface GLView : UIView {
    EAGLContext* m_context;
    IRenderingEngine* m_renderingEngine;
    float m_timestamp;
}

- (void) drawView: (CADisplayLink*) displayLink;
- (void) didRotate: (NSNotification*) notification;

@end
