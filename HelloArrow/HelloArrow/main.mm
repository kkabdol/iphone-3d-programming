//
//  main.m
//  HelloArrow
//
//  Created by Seonghyeon Choe on 11/2/14.
//  Copyright (c) 2014 shaun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
