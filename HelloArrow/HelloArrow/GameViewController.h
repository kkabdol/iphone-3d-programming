//
//  GameViewController.h
//  HelloArrow
//
//  Created by Seonghyeon Choe on 11/2/14.
//  Copyright (c) 2014 shaun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

@interface GameViewController : GLKViewController

@end
