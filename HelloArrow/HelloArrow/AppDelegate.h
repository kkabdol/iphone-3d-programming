//
//  AppDelegate.h
//  HelloArrow
//
//  Created by Seonghyeon Choe on 11/2/14.
//  Copyright (c) 2014 shaun. All rights reserved.
//

#import "GLView.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
@private
    UIWindow* m_window;
    GLView* m_view;
}

@end

